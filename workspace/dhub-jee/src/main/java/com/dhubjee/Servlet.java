/**
 * 
 */
package com.dhubjee;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Etienne
 * 
 */
@WebServlet(value = "/Servlet", name = "Servlet")
public class Servlet extends HttpServlet {

	// URL de la servlet : http://localhost:8080/DhubDemo/Servlet
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7426324721238170538L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		doPost(request, response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) {

		System.setProperty("http.proxySet", "true");
		System.setProperty("http.proxyHost", "proxy.ensicaen.fr");
		System.setProperty("http.proxyPort", "3128");

		try {
			response.getWriter().println("Test de la servlet");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
