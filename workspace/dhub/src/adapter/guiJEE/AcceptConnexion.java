/**
 * 
 */
package adapter.guiJEE;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


import dhub.service.IOManager;


/**
 * @author colangla
 *
 */
public class AcceptConnexion implements Runnable{

	 private ServerSocket mSocketserver = null;
	 private Socket mSocket = null;	 
	 public Thread mT1;
	 private boolean mIsRunning = false; 
	 private ComWithClient comClient = null;
	 private IOManager iomanager = null;
	 
	
	/**
	 * 
	 */
	public AcceptConnexion(ServerSocket pServerSocket, IOManager iomanager) {
		
		this.mSocketserver = pServerSocket;
		this.iomanager = iomanager;
	}
	
	/*
	 * Get / Set
	 */
	public boolean isIsRunning() {
		return mIsRunning;
	}

	public void setIsRunning(boolean mIsRunning) {		
		this.mIsRunning = mIsRunning;		
	}
	
	/*
	 * Methods
	 */
	public void run(){
		
		try {
			mIsRunning = true;
			
            while(mIsRunning){
                 
            	mSocket = mSocketserver.accept();
            	System.out.println("New connection.");	
            	
            	comClient = new ComWithClient(mSocket,iomanager);
            	mT1 = new Thread(comClient);
            	mT1.start();             
            }
        } catch (IOException e) {
        	System.out.println(e.getMessage());
        }
		
	}
	
	public void close(){
		
		try{
			mIsRunning = false;
			if(comClient != null){
				comClient.close();	
			}else{
				if(mSocket != null){
					mSocket.close();
				}				
			}
			mSocketserver.close();
		}catch(IOException e){
			System.out.println(e.getMessage());			
		}
	}
}
