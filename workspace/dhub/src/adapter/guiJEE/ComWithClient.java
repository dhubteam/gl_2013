/**
 * 
 */
package adapter.guiJEE;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


import dhub.business.impl.device.DeviceCommand;
import dhub.service.Device;
import dhub.service.IOManager;



/**
 * @author colangla
 *
 */
public class ComWithClient implements Runnable {

	
	private Socket mSocket;
	private PrintWriter mOut = null;
    private BufferedReader mIn = null;
    private Boolean mIsRunning = false;
    private Emission mEmission = null;
    private IOManager iomanager = null;
    
    
	/**
	 * 
	 */
	public ComWithClient(Socket pSocket, IOManager iomanager) {		
		this.mSocket = pSocket;
		this.iomanager = iomanager;
	}
	
	public void run(){
		
		try {
			String message = "";
			
			mOut = new PrintWriter(mSocket.getOutputStream());
			mIn = new BufferedReader(new InputStreamReader(mSocket.getInputStream()));
			mEmission = new Emission(mOut);
             
			mIsRunning = true;
			
			while(mIsRunning){				
				
				message = "";
				message = mIn.readLine();
				
				
				if(message != null){
					System.out.println("Message in input : " + message);
					message = message + ";";
								
					
					int length = 0;
					String subString = "";
					length = message.length();
					ArrayList<String> stringList = new ArrayList<String>();
					
					while (length > 1) {
						
						subString = message.substring(0, message.indexOf(";"));
						message = message.substring(message.indexOf(";") + 1,length);
						length = message.length();

						stringList.add(subString);
					}
						
					DeviceCommand clientCommand = new DeviceCommand();
					boolean senderIsVerified = false;
					
					for (int i = 0; i < iomanager.getGuiList().size(); i++) {
	            		
	            		if(iomanager.getGuiList().get(i) instanceof GuiJEEAdapter){
	            			((GuiJEEAdapter) iomanager.getGuiList().get(i)).setmSocket(mSocket);	            			
	            		}  
	            		
	            		if(iomanager.getGuiList().get(i).getId().equals(stringList.get(0))){
	            			clientCommand.set_sender(iomanager.getDeviceList().get(i));
	            			senderIsVerified = true;
	            		}
	            		
					}
					
					for (int i = 0; i < iomanager.getDeviceList().size(); i++) {
	            		
	            		if(iomanager.getDeviceList().get(i).getId().equals(stringList.get(1))){
	            			clientCommand.set_reveiver(iomanager.getDeviceList().get(i));
	            		}  
					}
					
					clientCommand.set_action(stringList.get(2));
					
					if(senderIsVerified){
						iomanager.ForwardCommand(clientCommand);
					}
				}
				
			}
                       
        } catch (IOException e) {
        	
        	System.out.println(e.getMessage());	
        }
		
	}
	
	public void close(){
				
		try{
			mIsRunning = false;
			
			if(mOut != null){
				mOut.close();
			}
			
			if(mIn != null){
				mIn.close();
			}
			
		}catch(IOException e){
			System.err.println("Error close Server_ComWithClient");
		}
	}

}
