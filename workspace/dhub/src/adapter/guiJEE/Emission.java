/**
 * 
 */
package adapter.guiJEE;

import java.io.PrintWriter;

/**
 * @author colangla
 *
 */
public class Emission {
	
	private PrintWriter mOut;
	/**
	 * 
	 */
	public Emission(PrintWriter pOut) {
		mOut = pOut;
	}
	
	public void send(String pMessage){
		mOut.println(pMessage);
		mOut.flush();
	}

}
