package adapter.guiJEE;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import dhub.business.Command;
import dhub.business.State;
import dhub.service.Device;
import dhub.service.IOManager;

public class GuiJEEAdapter extends Device{

	// Network variables
	private static ServerSocket mServerSocket = null;
	private static Thread mT;
	private AcceptConnexion mAcceptConnexion;
	private IOManager iomanager = null;
	private int port = 0;
	private Socket mSocket = null;  
	
	public GuiJEEAdapter(String id, IOManager iomanager, int port) {
		super(id);
		this.iomanager = iomanager;
		this.port = port;
	}
	
	public Socket getmSocket() {
		return mSocket;
	}

	public void setmSocket(Socket mSocket) {
		if(this.mSocket == null){
			this.mSocket = mSocket;	
		}
	}
	
	@Override
	public void Start(){
		try {
					
			mServerSocket = new ServerSocket(this.port);		
			mAcceptConnexion = new AcceptConnexion(mServerSocket,iomanager);
			mT = new Thread(mAcceptConnexion);
			mT.start();			
			System.out.println("Server launch.");	

		} catch (IOException e) {
			System.out.println(e.getMessage());			
		}
	}
	
	@Override
	public void Stop(){
		
			mAcceptConnexion.close();	
			System.out.println("Server stop.");				
	}

	@Override
	public void Notify(Device device) {
		PrintWriter out;
		try {
			
			if(mSocket != null){
				System.out.println("Notify GUIJEE : " + device.GetState().getValue());
				
				out = new PrintWriter(mSocket.getOutputStream());
				Emission emission = new Emission(out);			
				out.println(device.getId() + ";" + this.getId() + ";" + device.GetState().getValue());
				out.flush();
			}
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		
	}

	@Override
	public void ReceiveCommand(Command command) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean IsAlive() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public State GetState() {
		// TODO Auto-generated method stub
		return null;
	}

}
