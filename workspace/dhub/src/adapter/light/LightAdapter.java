package adapter.light;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


import j.extensions.comm.SerialComm;
import dhub.business.Command;
import dhub.business.State;
import dhub.service.Device;
import dhub.service.IOManager;

public class LightAdapter extends Device{

	private SerialComm  serialPorts[];
	private OutputStream streamOut;
	private InputStream streamIn;
	private IOManager iomanager;
	
	public LightAdapter (String id, IOManager iomanager) {
		
		super(id);
		this.iomanager = iomanager;
	}

	public void Notify(Device device) {
		
		iomanager.Notify(device);		
	}

	public void ReceiveCommand(Command command) {
		
		try {
			
			System.out.println("Envoi de la commande : " + command.get_action().charAt(0));
			streamOut.write(command.get_action().charAt(0));

			int response = streamIn.read();
			System.out.println("Retour = " + response);
						
			Notify(this);
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	public boolean IsAlive() {

		int result = 0;
			
		try {
			streamOut.write('v');
			result = streamIn.read();
			
		} catch (IOException e) {

			e.printStackTrace();
		}
		
		return result > 0;

	}

	public State GetState() {
		
		int result = 0;
		
		State lightState = null;
		
		try {
			streamOut.write('e');
			result = streamIn.read();
			
			System.out.println("result : " + result);
			
			if(result == 48){
				lightState = LightStates.touteteint;
			}else if(result == 49){
				lightState = LightStates.bleuAllume;
			}else if(result == 50){
				lightState = LightStates.rougeAllume;
			}else {
				lightState = LightStates.toutAllume;
			}
			
			
		} catch (IOException e) {

			e.printStackTrace();
		}
		
		return lightState;
	}

	@Override
	public void Start() {
		
		serialPorts = SerialComm.getCommPorts();
		serialPorts[0].openPort();
		streamOut = serialPorts[0].getOutputStream();
		streamIn = serialPorts[0].getInputStream();
		
	}

	@Override
	public void Stop() {
		
		serialPorts[0].closePort();
		
		try {
			streamIn.close();
			streamOut.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
