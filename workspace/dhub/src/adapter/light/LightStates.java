package adapter.light;

import dhub.business.State;

public enum LightStates implements State {
	touteteint('0'),
	bleuAllume('1'),
	rougeAllume('2'),
	toutAllume('3');
	
	private char value;
	
	private LightStates(char value) {
		this.value = value;
	}
	
	public char getValue() {
		return this.value;
	}
}
