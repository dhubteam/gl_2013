package demo;

import java.util.ArrayList;

import dhub.service.Device;
import dhub.service.IOManager;
import adapter.guiJEE.GuiJEEAdapter;
import adapter.light.LightAdapter;

public class Demo {

	public static void main(String[] args) {
		try{
			ArrayList<Device> deviceList = new ArrayList<Device>();
			ArrayList<Device> guiList = new ArrayList<Device>();
			
			IOManager ioManager = new IOManager(deviceList, guiList);	
			ioManager.getDeviceList().add(new LightAdapter("LIGHT-1",ioManager));
			ioManager.getGuiList().add(new GuiJEEAdapter("JEE-IHM",ioManager, 26263));
			
			ioManager.Start();
			
		}catch(Exception e){
			System.out.println(e.getMessage());
		}

	}

}
