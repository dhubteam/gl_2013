package dhub.business;

import dhub.service.Device;

public abstract class Command {
	
	private Device _sender;
	private Device _reveiver;
	private String _action;
	
	public Command () {
				
	}
	
	public Command (Device _sender, Device _reveiver, String _action) {
		
		this._sender = _sender;
		this._reveiver = _reveiver;
		this._action = _action;
		
	}
	
	public abstract void execute(String command );

	public Device get_sender() {
		return _sender;
	}

	public void set_sender(Device _sender) {
		this._sender = _sender;
	}

	public Device get_reveiver() {
		return _reveiver;
	}

	public void set_reveiver(Device _reveiver) {
		this._reveiver = _reveiver;
	}

	public String get_action() {
		return _action;
	}

	public void set_action(String _action) {
		this._action = _action;
	}
	
}
