package dhub.service;

import java.net.Socket;
import java.util.ArrayList;

import dhub.business.Command;
import dhub.business.CommandColleague;
import dhub.business.DeviceObservable;
import dhub.business.State;

public abstract class Device extends DeviceObservable implements CommandColleague {
	
	private ArrayList<State> stateList = new ArrayList<State>();		
	private String id = "";
	
	public Device (String id){
		this.id = id;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ArrayList<State> getStateList() {
		return stateList;
	}

	public void setStateList(ArrayList<State> stateList) {
		this.stateList = stateList;
	}

	public abstract void Notify (Device device);
	
	public abstract void ReceiveCommand(Command command);
	
	public abstract boolean IsAlive();
	
	public abstract State GetState();
	
	public abstract void Start();
	
	public abstract void Stop();

}
