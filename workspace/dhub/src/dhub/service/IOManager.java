package dhub.service;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;

import adapter.guiJEE.GuiJEEAdapter;
import adapter.light.LightAdapter;
import dhub.business.*;
import dhub.business.impl.device.CommandQueue;
import dhub.business.impl.device.DeviceCommand;

public class IOManager extends DeviceObserver implements CommandMediator {
	
	private ArrayList<CommandQueue> queueList = new ArrayList<CommandQueue>();
	private ArrayList<Device> deviceList = new ArrayList<Device>();
	private ArrayList<Device> guiList = new ArrayList<Device>();
	
	
	
	public IOManager(ArrayList<Device> deviceList, ArrayList<Device> guiList){
		this.deviceList = deviceList;
		this.guiList = guiList;
		
	}
	
	public ArrayList<Device> getGuiList() {
		return guiList;
	}

	public void setGuiList(ArrayList<Device> guiList) {
		this.guiList = guiList;
	}

	public ArrayList<CommandQueue> getQueueList() {
		return queueList;
	}

	public void setQueueList(ArrayList<CommandQueue> queueList) {
		this.queueList = queueList;
	}

	public ArrayList<Device> getDeviceList() {
		return deviceList;
	}

	public void setDeviceList(ArrayList<Device> deviceList) {
		this.deviceList = deviceList;
	}

	public void Notify(Device device){
		
		for (Device gui : guiList) {
			gui.Notify(device);
		}
				
	}
	
	public void ForwardCommand(Command command){
		
		if(command.get_reveiver() != null){
			
			// Send command to receiver
			command.get_reveiver().ReceiveCommand(command);
		}
	}	
	
	public void Start(){
		
		for (Device device : guiList) {
			device.Start();
		}
		
		for (Device device : deviceList) {
			device.Start();
		}
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				while(true);
				
			}
		}).start();
	}
	
	

}
